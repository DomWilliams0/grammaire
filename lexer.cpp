#include <string>
#include <cctype>
#include <unordered_map>
#include "lexer.hpp"

Lexer::Lexer(std::istream &is) : stream(is)
{
}

Lexer::~Lexer()
{
	for (Symbole *s : symbols)
		delete s;
}

Symbole *Lexer::lireEntier(char commence)
{
	std::string str;

	if (commence)
		str += commence;

	char c;
	while (true)
	{
		int peek = stream.peek();
		if (peek == EOF || !isdigit(peek)) 
			break;

		stream >> c;
		str += c;
	}

	if (str.empty())
		return nullptr;

	int i = std::stoi(str);
	return new RawSymbole(i, ENTIER);
}

bool Lexer::read()
{
	static std::unordered_map<char, SymboleType> SYMBOL_TYPES = {
		{'+', PLUS},
		{'*', MULT},
		{'(', OUVREPAR},
		{')', FERMEPAR}
	};

	char c; 
	Symbole *s, *nextSymbole;

	while (stream >> std::noskipws >> c)
	{
		switch(c){
			case ' ':
			case'\t':
			case'\n':
				break;

			case '+':
			case '*':
			case '(':
			case ')':
				s = new RawSymbole(c, SYMBOL_TYPES[c]);
				nextSymbole = lireEntier();

				symbols.push_back(s);
				if (nextSymbole)
					symbols.push_back(nextSymbole);

				break;

			default:
				if (!isdigit(c))
				{
					std::cerr << "Erreur de lecture: " << c << std::endl;
					return false;
				}

				nextSymbole = lireEntier(c);

				if (nextSymbole)
					symbols.push_back(nextSymbole);
				break;
		}

	}

	symbols.push_back(new RawSymbole('$', DOLLAR));

	return true;
}

void Lexer::consumeHead()
{
	if (!symbols.empty())
	{
		symbols.pop_front();
	}
}

Symbole *Lexer::peekHead()
{
	if (symbols.empty())
	{
		std::cerr << "Lexer failed to peek token" << std::endl;
		return nullptr;
	}

	return symbols.front();
}

void Lexer::putSymbol(Symbole *s)
{
	symbols.push_front(s);
}
