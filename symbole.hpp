#ifndef SYMBOLE_H
#define SYMBOLE_H

enum SymboleType
{
	OUVREPAR,
	FERMEPAR,
	PLUS,
	MULT,
	ENTIER,

	EXPR,
	DOLLAR
};

class Symbole
{
	protected : 	
		SymboleType type;

		virtual void printSelf() const = 0;

	public : 
		Symbole(SymboleType type) : type(type) {}
		virtual ~Symbole() {}

		void print(bool newLine = true) const;
		SymboleType getType() const;
};

class RawSymbole : public Symbole
{
	protected:
		int valeur;

		void printSelf() const;

	public:
		RawSymbole(int valeur, SymboleType type) : Symbole(type),  valeur(valeur) {}

		operator int() const { return valeur; }
};

class Expr : public Symbole
{
	public:
		Expr() : Symbole(EXPR) {}
		~Expr() {}

		virtual int eval() const = 0;
};

class ExprNombre : public Expr
{
	protected:
		int valeur;

	public:
		ExprNombre(int valeur) : valeur(valeur) {}
		int eval() const;
		void printSelf() const;

		operator int() const { return valeur; }
};

class ExprPar : public Expr
{
	protected:
		Expr *expr;

	public:
		ExprPar(Expr *expr) : expr(expr) {}
		~ExprPar();
		int eval() const;
		void printSelf() const;
};

class ExprBinaire : public Expr
{
	protected:
		Expr *left, *right;

	public:
		ExprBinaire(Expr *left, Expr *right) : left(left), right(right) {}
		~ExprBinaire();
		int eval() const;
		virtual int operate(int left, int right) const = 0;
};

class ExprPlus : public ExprBinaire
{
	public:
		ExprPlus(Expr *left, Expr *right) : ExprBinaire(left, right) {}
		void printSelf() const;
		int operate(int left, int right) const;
};

class ExprMult : public ExprBinaire
{
	public:
		ExprMult(Expr *left, Expr *right) : ExprBinaire(left, right) {}
		void printSelf() const;
		int operate(int left, int right) const;
};

#endif
