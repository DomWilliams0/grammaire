#ifndef LEXER_H
#define LEXER_H

#include <iostream>
#include <stack>
#include "symbole.hpp"

class Lexer
{
	public:
		Lexer(std::istream &is);
		~Lexer();

		bool read();

		void consumeHead();
		Symbole *peekHead();
		void putSymbol(Symbole *s);

	protected:
		std::deque<Symbole *> symbols;
		std::istream &stream;

		Symbole *lireEntier(char commence = '\0');
};

#endif
