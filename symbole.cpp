#include <iostream>
#include "symbole.hpp"

static void printType(const SymboleType &type)
{
	switch(type)
	{
		case OUVREPAR:
			std::cout << "OUVREPAR";
			break;
		case FERMEPAR:
			std::cout << "FERMEPAR";
			break;
		case PLUS:
			std::cout << "PLUS";
			break;
		case MULT:
			std::cout << "MULT";
			break;
		case ENTIER:
			std::cout << "ENTIER";
			break;
		case EXPR:
			std::cout << "EXPR";
			break;
		case DOLLAR:
			std::cout << "DOLLAR";
			break;
	}
}

void Symbole::print(bool newLine) const
{
	std::cout << "Symbole(type=";
	printType(type);

	printSelf();
	std::cout << ")";

	if (newLine)
		std::cout << std::endl;
}

SymboleType Symbole::getType() const
{
	return type;
}

int ExprNombre::eval() const
{
	return valeur;
}

int ExprPar::eval() const
{
	return expr->eval();
}

int ExprBinaire::eval() const
{
	return operate(left->eval(), right->eval());
}

int ExprPlus::operate(int left, int right) const
{
	return left + right;
}

int ExprMult::operate(int left, int right) const
{
	return left * right;
}

void RawSymbole::printSelf() const
{
	std::cout << ", value=";
	if (type == ENTIER)
		std::cout << valeur;
	else
		std::cout << "'" << static_cast<char>(valeur) << "'";
}

void ExprNombre::printSelf() const
{
	std::cout << "-NOMBRE(" << valeur << ")";
}

void ExprPlus::printSelf() const
{
	std::cout << "-PLUS(";
	left->print(false);
	std::cout << ", ";
	right->print(false);
	std::cout << ")";
}

void ExprMult::printSelf() const
{
	std::cout << "-MULT(";
	left->print(false);
	std::cout << ", ";
	right->print(false);
	std::cout << ")";
}

void ExprPar::printSelf() const
{
	std::cout << "-PAR(";
	expr->print(false);
	std::cout << ")";
}

ExprPar::~ExprPar()
{
	if (expr)
		delete expr;

}

ExprBinaire::~ExprBinaire()
{
	if (left)
		delete left;
	if (right)
		delete right;
}
