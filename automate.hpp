#ifndef AUTOMATE_H
#define AUTOMATE_H

#include <iostream>
#include <stack>
#include "symbole.hpp"
#include "etat.hpp"
#include "lexer.hpp"

class Automate
{
	protected:
		std::stack<Etat *> stateStack;
		std::stack<Symbole *> symbolStack;

		Lexer lexer;
		bool accepted;

	public:
		Automate(std::istream &is) : lexer(is), accepted(false) {}
		~Automate();

		void putSymbol(Symbole *s);
		Symbole *popSymbol();
		void popAndDestroySymbol();
		void decalage(Symbole *s, Etat *e);
		void reduction(int n, Symbole *s);
		void accept();

		bool parse(int &out);
};
#endif
