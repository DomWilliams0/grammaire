/*
 * main.cpp
 * 
 * Copyright 2017 Altaha Dalya <daltaha@if501-219-05.insa-lyon.fr>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include <iostream>
#include "automate.hpp"

int main(int argc, char **argv)
{
	int result;

	Automate automate(std::cin);
	if (!automate.parse(result))
	{
		std::cerr << "Parse error" << std::endl;
		return 1;
	}

	std::cout << "Result: " << result << std::endl;
	return 0;
}
