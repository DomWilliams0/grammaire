#include "automate.hpp"

Automate::~Automate()
{
	while (!stateStack.empty())
	{
		delete stateStack.top();
		stateStack.pop();
	}

	while (!symbolStack.empty())
	{
		delete symbolStack.top();
		symbolStack.pop();
	}
}

void Automate::decalage(Symbole *s, Etat *e)
{
	symbolStack.push(s);
	stateStack.push(e);

	lexer.consumeHead();
}

void Automate::putSymbol(Symbole *s)
{
	symbolStack.push(s);
}

Symbole *Automate::popSymbol()
{
	if (symbolStack.empty())
	{
		std::cerr << "Automate failed to pop symbol" << std::endl;
		return nullptr;
	}

	Symbole *s = symbolStack.top();
	symbolStack.pop();
	return s;
}

void Automate::popAndDestroySymbol()
{
	Symbole *s = popSymbol();
	if (s)
		delete s;
}


void Automate::reduction(int n, Symbole *s)
{
	for (int i = 0; i < n && !stateStack.empty(); ++i)
	{
		delete stateStack.top();
		stateStack.pop();
	}

	lexer.putSymbol(s);
}

void Automate::accept()
{
	accepted = true;
}

bool Automate::parse(int &out)
{
	if (!lexer.read())
		return false;

	// init state stack
	stateStack.push(new E0);

	bool error = false;
	while (!accepted)
	{
		if (stateStack.empty())
		{
			error = true;
			break;
		}

		Etat *current = stateStack.top();
		Symbole *head = lexer.peekHead();

		if (!head)
		{
			error = true;
			break;
		}

		bool transitioned = current->transition(*this, head);
		if (!transitioned)
		{
			std::cerr << "Invalid state" << std::endl;
			error = true;
			break;
		}
	}

	if (error || symbolStack.empty())
		return false;

	Expr *expr = (Expr *)symbolStack.top();
	out = expr->eval();
	return true;
}
