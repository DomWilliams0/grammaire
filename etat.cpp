#include <iostream>
#include "automate.hpp"
#include "etat.hpp"
#include "symbole.hpp"

const std::string &Etat::getName() const
{
	return name;
}

void Etat::print() const
{
	std::cout << "Etat(" << name << ")" << std::endl;
}

bool E0::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case ENTIER:
			a.decalage(s, new E3);
			return true;

		case OUVREPAR:
			a.decalage(s, new E2);
			return true;

		case EXPR:
			a.decalage(s, new E1);
			return true;

		default:
			return false;
	}
}

bool E1::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case PLUS:
			a.decalage(s, new E4);
			return true;

		case MULT:
			a.decalage(s, new E5);
			return true;

		case DOLLAR:
			a.accept();
			return true;

		default:
			return false;
	}
}

bool E2::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case ENTIER:
			a.decalage(s, new E3);
			return true;

		case OUVREPAR:
			a.decalage(s, new E2);
			return true;

		case EXPR:
			a.decalage(s, new E6);
			return true;

		default:
			return false;
	}
}

bool E3::transition(Automate &a, Symbole *s)
{
	RawSymbole *val  = static_cast<RawSymbole *>(a.popSymbol());
	a.reduction(1, new ExprNombre(*val));
	delete val;

	return true;
}

bool E4::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case ENTIER:
			a.decalage(s, new E3);
			return true;

		case OUVREPAR:
			a.decalage(s, new E2);
			return true;

		case EXPR:
			a.decalage(s, new E7);
			return true;

		default:
			return false;
	}
}

bool E5::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case ENTIER:
			a.decalage(s, new E3);
			return true;

		case OUVREPAR:
			a.decalage(s, new E2);
			return true;

		case EXPR:
			a.decalage(s, new E8);
			return true;

		default:
			return false;
	}
}

bool E6::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case PLUS:
			a.decalage(s, new E4);
			return true;

		case MULT:
			a.decalage(s, new E5);
			return true;

		case FERMEPAR:
			a.decalage(s, new E9);
			return true;

		default:
			return false;
	}
}

bool E7::transition(Automate &a, Symbole *s)
{
	switch(s->getType())
	{
		case MULT:
			a.decalage(s, new E5);
			return true;

		default:
			{
				Expr *left  = (Expr *)(a.popSymbol());
				a.popAndDestroySymbol();
				Expr *right  = (Expr *)(a.popSymbol());

				a.reduction(3, new ExprPlus(right, left));
				return true;
			}

	}
}

bool E8::transition(Automate &a, Symbole *s)
{
	Expr *left  = (Expr *)(a.popSymbol());
	a.popAndDestroySymbol();
	Expr *right  = (Expr *)(a.popSymbol());

	a.reduction(3, new ExprMult(right, left));
	return true;
}

bool E9::transition(Automate &a, Symbole *s)
{
	a.popAndDestroySymbol();
	Expr *expr  = (Expr *)(a.popSymbol());
	a.popAndDestroySymbol();

	a.reduction(3, new ExprPar(expr));
	return true;
}
