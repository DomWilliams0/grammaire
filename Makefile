CC = g++
CFLAGS = --std=c++11 -Wall -pedantic -O1

TARGET = calculator
OBJDIR = obj

SRC = $(shell find . -type f -name "*.cpp")
HDR = $(shell find . -type f -name "*.hpp")
OBJ = $(addprefix $(OBJDIR)/, $(SRC:.cpp=.o))

.PHONY: default
default: $(TARGET)

$(OBJDIR):
	@mkdir -p $(OBJDIR)

$(TARGET): $(OBJ) $(HDR)
	$(CC) $(CFLAGS) $^ -o $@

$(OBJDIR)/%.o: %.cpp | $(OBJDIR)
	$(CC) $(CFLAGS) $^ -o $@ -c

clean:
	@rm -rf $(OBJDIR) $(TARGET)

examples: $(TARGET)
	echo "(5+3)*(2*3+1)" | ./$(TARGET) # --> 56
	echo "(((2)))"       | ./$(TARGET) # --> 2
	echo "8"             | ./$(TARGET) # --> 8
	echo "4+2*3+1"       | ./$(TARGET) # --> 11
	echo -e "10\t+\n20 " | ./$(TARGET) # --> 30
	echo "20*3+"         | ./$(TARGET) # --> erreur

